# Navigation

Navigation is a technique project to take a close look at navigation in SwiftUI.

This app was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/navigation-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- navigation presentation values and the navigationDestination()
- NavigationStack
- Programmatic navigation
- NavigationPath
- Codable support
- Custom navigation bar appearances
- Hiding the back button
- Toolbar placements