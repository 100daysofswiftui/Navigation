//
//  NavigationApp.swift
//  Navigation
//
//  Created by Pascal Hintze on 10.11.2023.
//

import SwiftUI

@main
struct NavigationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
